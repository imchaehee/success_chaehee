from django.apps import AppConfig


class UserRentalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'user_rental'
