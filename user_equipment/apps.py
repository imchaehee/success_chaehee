from django.apps import AppConfig


class UserEquipmentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'user_equipment'
